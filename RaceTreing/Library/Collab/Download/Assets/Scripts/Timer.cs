﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Timer : MonoBehaviour
{
    TextMeshProUGUI timer;
    float minutes;
    float secounds;
    float resetTimer;

    bool isTimer;
    
    // Start is called before the first frame update
    void Start()
    {
        timer = GetComponent<TextMeshProUGUI>();
        //TimerTrue();
    }

    // Update is called once per frame
    void Update()
    {
        float timerTime = Time.time-resetTimer;
        minutes = Mathf.RoundToInt(timerTime / 60);
        secounds = Time.time % 60;
        if (isTimer)
        {
            timer.text = minutes.ToString("00") + ":" + secounds.ToString("00");

        }
    }

    public void TimerTrue()
    {
        isTimer = true;
    }
    public void TimerFalse()
    {
        isTimer = false;
    }
    public void ResetTimer()
    {
        resetTimer = Time.time;
        TimerTrue();
    }

    public string GetCurretTimerAsString()
    {
        return minutes.ToString("00" + ":" + secounds.ToString("00"));
    }
}
