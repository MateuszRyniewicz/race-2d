﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;

public class PanelConfig : MonoBehaviour
{
    [SerializeField]
    TextMeshProUGUI _textNumberOfValue;

    [SerializeField]
    Slider slider;

    [SerializeField]
    GameObject[] _playerPrefabs;

    [SerializeField]
    GameObject[] _aiCarPrefabs;


    [SerializeField]
    Map[] _maps;

    [SerializeField]
    TextMeshProUGUI _mapNameText;

    [SerializeField]
    Image _imageSprites;

    LabSystem labSystem;


    public MapContainer _mapContainer;


    int curretMapIndex = 0;
    int playerCarNumber;
    int aiCarNumber;


    void Start()
    {
        labSystem = GameObject.FindObjectOfType<LabSystem>();
        Time.timeScale = 0;
        _mapNameText.text= _maps[0].MapName;
    }

   public void NextMap()
    {
        if (curretMapIndex < _maps.Length - 1)
        {
            curretMapIndex++;
        }
        _mapNameText.text = _maps[curretMapIndex].MapName;
        _imageSprites.sprite = _maps[curretMapIndex].thumbnali;
    }

    public void PreviousMap()
    {
        if (curretMapIndex > 0)
        {
            curretMapIndex--;
        }

        _mapNameText.text = _maps[curretMapIndex].MapName;
        _imageSprites.sprite = _maps[curretMapIndex].thumbnali;
    }

    public void UpdateTextValue(float value)
    {
        _textNumberOfValue.text = value.ToString();
    }

    public void StartCoroutine()
    {
        StartCoroutine(StartButton());
    }

    public IEnumerator StartButton()
    {
        SceneManager.LoadScene(_maps[curretMapIndex].MapFileName,LoadSceneMode.Additive);

        if (_mapContainer == null)
        {
            yield return null;
        }
     
        labSystem.ConfigurateRace(Mathf.RoundToInt(slider.value));
        this.gameObject.SetActive(false);
        SetUpAiCar();
        SetUpPlayerCar();
        Time.timeScale = 1;
    }

    public void SetUpNumberPlayerCar(int numberCar)
    {
        playerCarNumber = Mathf.Clamp(numberCar, 0, _playerPrefabs.Length - 1);
    }

    public void SetUpPlayerCar()
    {
        Instantiate(_playerPrefabs[playerCarNumber],_mapContainer._startPoints[0].position, Quaternion.identity);
    }
    public void SetUpAiCar()
    {
        Instantiate(_aiCarPrefabs[Random.Range(0, _aiCarPrefabs.Length)], _mapContainer._startPoints[1].position, Quaternion.identity);
        Instantiate(_aiCarPrefabs[Random.Range(0, _aiCarPrefabs.Length)], _mapContainer._startPoints[2].position, Quaternion.identity);
        Instantiate(_aiCarPrefabs[Random.Range(0, _aiCarPrefabs.Length)], _mapContainer._startPoints[3].position, Quaternion.identity);
    }
}
