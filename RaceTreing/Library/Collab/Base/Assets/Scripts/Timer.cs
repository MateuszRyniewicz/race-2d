﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine.UI;
using UnityEngine;

public class Timer : MonoBehaviour
{
    [SerializeField]
    TextMeshProUGUI timer;
    float minutes;
    float secodns;
    float _resetTime;

    bool isTimer;
    // Start is called before the first frame update
    void Start()
    {
        timer = GameObject.FindGameObjectWithTag("Timer").GetComponent<TextMeshProUGUI>();
       // TimerTrue();
    }

    // Update is called once per frame
    void Update()
    {
        float _timer = Time.time-_resetTime;
        minutes = Mathf.RoundToInt(_timer / 60);
        secodns = _timer % 60;

        if (isTimer)
        {
            timer.text = minutes.ToString("00")+":"+ secodns.ToString("00");
        }
    }

    public void TimerTrue()
    {
        isTimer = true;
    }
    public void TimerFalse()
    {
        isTimer = false;
    }
    public string GetTextToString()
    {
        return timer.text = minutes.ToString("00") + ":" + secodns.ToString("00");
    }
    public void TimerReset()
    {
        _resetTime = Time.time;
        TimerTrue();
    }

    public void FixedUpdate()
    {
        Debug.Log("Timer");
    }
}
