﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LabSystem : MonoBehaviour
{
    

    [SerializeField]
    GameObject _panelRestart;
    [SerializeField]
    TextMeshProUGUI _labTimeText;

    [SerializeField]
    int allLabs = 3;
    [SerializeField]
    CheckPoint []_allPoints;
    int curretPoint;
    int curretLab;

    Timer timer;
    

    void Start()
    {
        timer = GameObject.FindGameObjectWithTag("Timer").GetComponent<Timer>();
        
    }

   

    internal void CheckPointCompleted(CheckPoint checkedPoint)
    {
        if (checkedPoint == _allPoints[curretPoint])
        {
            if (curretPoint == 0)
            {
                curretLab++;

                if (curretLab > allLabs)
                {
                    RaceCompleted();
                }
                else
                {
                    ShowTime();
                }
            }

            curretPoint++;

            if (curretPoint > _allPoints.Length - 1)
            {
                curretPoint = 0;
            }
        }
       
    }

    internal void ConfigurateRace(int _labsValue, CheckPoint[] _allpoints)
    {
        allLabs = _labsValue;
        timer.ResetTimer();
        curretLab = 0;
        curretPoint = 0;
        this._allPoints = _allpoints;
    }

    public void RaceCompleted()
    {
        _panelRestart.SetActive(true);
        timer.TimerFalse();
        Player player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        player._playerSpeed = 0;
        
       
    }
    public void ShowTime()
    {
        _labTimeText.text= timer.GetCurretTimerAsString();
        _labTimeText.enabled = true;
        Invoke("HiddenLabTimeText",5f);
    }
    public void HiddenLabTimeText()
    {
        _labTimeText.enabled = false;
    }
}
