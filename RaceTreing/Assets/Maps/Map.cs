﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Map",menuName ="Map")]
public class Map : ScriptableObject
{
    public string MapName;
    public Sprite thumbnali;
    public string MapFileName;
}
