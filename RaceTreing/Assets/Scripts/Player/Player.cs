﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    Rigidbody2D _rb;

    [SerializeField]
    public float _playerSpeed=4;

    [SerializeField]
    float _playerRotate = 120f;

    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();   
    }

  
    void FixedUpdate()
    {
        MovingPlayer();
        RotatePlayer();
    }
    public void MovingPlayer()
    {
        float vertical = Input.GetAxis("Vertical");
        Vector3 newPos = transform.position + transform.up * -0.4f;
        _rb.AddForceAtPosition(transform.up * _playerSpeed * vertical, newPos);
    }
    public void RotatePlayer()
    {
        float horizontal = Input.GetAxis("Horizontal");

        if (_rb.velocity.magnitude > 1)
        {
            transform.Rotate(0, 0, -_playerRotate*horizontal*Time.deltaTime);
        }
        else
        {
            transform.Rotate(0, 0, -_playerRotate*horizontal*Time.deltaTime*_rb.velocity.magnitude);
        }
    }
}
