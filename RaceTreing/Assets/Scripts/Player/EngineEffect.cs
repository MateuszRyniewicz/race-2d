﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EngineEffect : MonoBehaviour
{
    AudioSource _audioSource;
    Rigidbody2D _rb;
    // Start is called before the first frame update
    void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        _rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        _audioSource.pitch = Mathf.Clamp(_rb.velocity.magnitude, 0, 2);
    }
}
