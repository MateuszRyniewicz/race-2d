﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriesEffect : MonoBehaviour
{
    Rigidbody2D _rb;
    [SerializeField]
    ParticleSystem _triesEffectParticle;
    [SerializeField]
    float _angleTriesEffectAllows = 40f;

    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _triesEffectParticle.Pause();
    }

    // Update is called once per frame
    void Update()
    {
        float angle = Vector3.Angle(transform.up, _rb.velocity);

        if(angle >_angleTriesEffectAllows && _triesEffectParticle.isPaused)
        {
            _triesEffectParticle.Play();
        }
        else
        {
            _triesEffectParticle.Pause();
        }
    }
}
