﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Analytics;
using Cinemachine;
using UnityEngine;

public class PanelConfig : MonoBehaviour
{

    
    /// ////////////////////////////////////////////////////////
    

   public static PanelConfig Instance { get; private set; }

    public void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    
    /// ///////////////////////////////////////////////////////
    


    [SerializeField]
    TextMeshProUGUI _textNumberOfValue;
   
    [SerializeField]
    Slider slider;
    [SerializeField]
    GameObject[] _playerPrefabs;
    [SerializeField]
    GameObject[] _aiCarPrefabs;
    [SerializeField]
    GameObject _lockedSymbol;
    [SerializeField]
    int unLockedMapCounter = 1;
    [SerializeField]
    List<StatusMap> mapsStatus;

    [SerializeField]
    Button _startButton;
    [SerializeField]
    Map[] _maps;
    [SerializeField]
    TextMeshProUGUI _mapNameText;
    [SerializeField]
    Image _imageSprites;

    

    [SerializeField]
    GameObject _resetPanel;

    public MapContainer _mapContainer;


    int curretMapIndex = 1;
    int playerCarNumber;
    int aiCarNumber;


    void Start()
    {
       
        Time.timeScale = 0;
        _mapNameText.text= _maps[0].MapName;
        PrefereMapView(curretMapIndex);
    }

   public void NextMap()
    {
        if (curretMapIndex < _maps.Length )
        {
            curretMapIndex++;
        }
   
        PrefereMapView(curretMapIndex);
    }

    public void PreviousMap()
    {
        if (curretMapIndex > 1)
        {
            curretMapIndex--;
        }

        PrefereMapView(curretMapIndex);
    }

    public void PrefereMapView(int curretMapIdx)
    {
        if (mapsStatus[curretMapIdx-1].isUnlocked)
        {
            _lockedSymbol.SetActive(false);
            _startButton.interactable = true;
        }
        else
        {
            _lockedSymbol.SetActive(true);
            _startButton.interactable=false;
        }

        _mapNameText.text = _maps[curretMapIndex-1].MapName;
        _imageSprites.sprite = _maps[curretMapIndex-1].thumbnali;
    }

    public void UpdateTextValue(float value)
    {
        _textNumberOfValue.text = value.ToString();
    }

    public void StartCoroutine()
    {
        
        StartCoroutine(StartButton());
    }

    public void RestartRaceCoroutine()
    {
        _resetPanel.SetActive(false);
        SceneManager.UnloadSceneAsync(_maps[curretMapIndex-1].MapFileName);
        this.gameObject.SetActive(true);
        StartCoroutine(StartButton());
    }


    public IEnumerator StartButton()
    {
        Scene levelScene= SceneManager.LoadScene(_maps[curretMapIndex-1].MapFileName, new LoadSceneParameters(LoadSceneMode.Additive));

        yield return new  WaitUntil(() => levelScene.isLoaded);   /// wait until when

        _mapContainer = FindObjectOfType<MapContainer>();
        if (_mapContainer == null)
        {
           Debug.LogError("MapContainer is null");
        }
     
        LabSystem.Instance.ConfigurateRace(Mathf.RoundToInt(slider.value),_mapContainer._allPoints);
        Time.timeScale = 1;
        AnalyticsEvent.GameStart();
        SetUpAiCar(levelScene);
        SetUpPlayerCar(levelScene);
        this.gameObject.SetActive(false);
    }

    public void SetUpNumberPlayerCar(int numberCar)
    {
        playerCarNumber = Mathf.Clamp(numberCar, 0, _playerPrefabs.Length - 1);
    }

    public void SetUpPlayerCar(Scene scene)
    {
        GameObject car= Instantiate(_playerPrefabs[playerCarNumber],_mapContainer._startPoints[0].position, Quaternion.identity);
        SceneManager.MoveGameObjectToScene(car, scene);
        LabSystem.Instance.RegisterPlayers(car);
        CinemachineVirtualCamera vCam = FindObjectOfType<CinemachineVirtualCamera>();
        vCam.Follow = car.transform;
        vCam.LookAt = car.transform;
    }
    public void SetUpAiCar(Scene scene)
    {
        GameObject aiCar= Instantiate(_aiCarPrefabs[Random.Range(0, _aiCarPrefabs.Length)], _mapContainer._startPoints[1].position, Quaternion.identity);
        SceneManager.MoveGameObjectToScene(aiCar,scene);
        LabSystem.Instance.RegisterPlayers(aiCar);
        aiCar= Instantiate(_aiCarPrefabs[Random.Range(0, _aiCarPrefabs.Length)], _mapContainer._startPoints[2].position, Quaternion.identity);
        SceneManager.MoveGameObjectToScene(aiCar, scene);
        LabSystem.Instance.RegisterPlayers(aiCar);
        aiCar= Instantiate(_aiCarPrefabs[Random.Range(0, _aiCarPrefabs.Length)], _mapContainer._startPoints[3].position, Quaternion.identity);
        SceneManager.MoveGameObjectToScene(aiCar, scene);
        LabSystem.Instance.RegisterPlayers(aiCar);
    }
   public void UnLockMap(int count)
    {
        unLockedMapCounter = Mathf.Clamp(unLockedMapCounter + count, 1, mapsStatus.Count);
        for (int i = 0; i < unLockedMapCounter; i++)
        {
            mapsStatus[i].isUnlocked = true;
        }
    
    }

    [System.Serializable]
    public class StatusMap
    {
        public Map map;
        public bool isUnlocked;
    }    

}
