﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine.Analytics;
using UnityEngine.Advertisements;
using UnityEngine;
using System.Linq;
using System.Text;

public class LabSystem : MonoBehaviour, IUnityAdsListener
{

    /// <summary>
    /// Singleton
    /// </summary>
    public static LabSystem Instance { get; private set; }

    public void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    /// /////////////////////////////////////////////////

    public List<Tuple<int, string>> players = new List<Tuple<int, string>>();


    [SerializeField]
    GameObject _panelRestart;
    [SerializeField]
    TextMeshProUGUI _playerPositionsText;
    [SerializeField]
    TextMeshProUGUI _labTimeText;

    [SerializeField]
    int allLabs = 3;
    [SerializeField]
    CheckPoint []_allPoints;
    int curretPoint;
     bool firstLab;
    int curretLab;
    bool isRaceFinish=false;

    Timer timer;

    bool adsIsReady;

    string _placemnet = "rewardedVideo";

    Dictionary<string, object> parameters = new Dictionary<string, object>();


    void Start()
    {
        timer = GameObject.FindGameObjectWithTag("Timer").GetComponent<Timer>();

        Advertisement.AddListener(this);
        Advertisement.Initialize("1234567", true);
    }

    public void RegisterPlayers( GameObject playerObject)
    {
        players.Add(new Tuple<int, string>(0, playerObject.name));
    }

   public void SortPlayer()
    {
        if (isRaceFinish)
        {
            return;
        }

        List<Tuple<int, string>> sortedPlayers = players.OrderByDescending(x => x.Item1).ToList();

        StringBuilder ranking = new StringBuilder();
        ranking.Append("Position Player");
        ranking.AppendLine();

        foreach (var item in sortedPlayers)
        {
            Debug.Log($"Player: {item.Item2}, Count: {item.Item1}.");
            ranking.Append($"{sortedPlayers.IndexOf(item)+1}-{item.Item2}");
            ranking.AppendLine();
        }
        _playerPositionsText.text = ranking.ToString();
        
    }

   

    internal void CheckPointCompleted(CheckPoint checkedPoint)
    {
        if (checkedPoint == _allPoints[curretPoint])
        {
            if (curretPoint == 0 && !firstLab)
            {
                curretLab++;

                if (curretLab > allLabs)
                {
                    RaceCompleted();
                }
                else
                {
                    ShowTime();
                }
            }

            curretPoint++;
            System.Tuple<int, string> tmpPlayers = LabSystem.Instance.players.Find(n => n.Item2.Contains("Player"));
            int indexOfTuple = Instance.players.FindIndex(x => x == tmpPlayers);
            int checkPointsCount =( curretLab * _allPoints.Length )+ curretPoint;
            tmpPlayers = new System.Tuple<int, string>(checkPointsCount, tmpPlayers.Item2);
            Instance.players.RemoveAt(indexOfTuple);
            Instance.players.Insert(indexOfTuple, tmpPlayers);
            SortPlayer();

            if (curretPoint > _allPoints.Length - 1)
            {
                curretPoint = 0;
                firstLab = false;
            }
        }
       
    }

    internal void ConfigurateRace(int _labsValue, CheckPoint[] _allpoints)
    {
        allLabs = _labsValue;
        timer.ResetTimer();
        firstLab = true;
        curretLab = 0;
        curretPoint = 0;
        isRaceFinish = false;
        this._allPoints = _allpoints;
    }

    public void RaceCompleted()
    {


        Advertisement.Show();


        _panelRestart.SetActive(true);
        timer.TimerFalse();
        AnalyticsEvent.ScreenVisit(ScreenName.Settings);
        AnalyticsEvent.GameOver();

        parameters["Labs"] = allLabs;
        parameters["Time"] = timer.GetCurretTimerAsString();
        Analytics.CustomEvent("Race_Summary",parameters);
        Player player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        player._playerSpeed = 0;
        isRaceFinish = true;
        
       
    }
    public void ShowTime()
    {
        _labTimeText.text= timer.GetCurretTimerAsString();
        _labTimeText.enabled = true;
        Invoke("HiddenLabTimeText",5f);
    }
    public void HiddenLabTimeText()
    {
        _labTimeText.enabled = false;
    }

    public void OnUnityAdsReady(string placementId)
    {
        Debug.Log("Advertisement is ready");
        if (_placemnet == placementId)
        {
            adsIsReady = true;
        }
    }

    public void ShowRewardedAds()
    {
        if (adsIsReady)
        {
            Advertisement.Show(_placemnet);

        }
    }

    public void OnUnityAdsDidError(string message)
    {
        Debug.LogError("The bug during advertisement");   
    }

    public void OnUnityAdsDidStart(string placementId)
    {
        Debug.Log("Advertisement is starting");
    }

    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        if (showResult == ShowResult.Finished)
        {
            Debug.Log("Nagorda");
            PanelConfig.Instance.UnLockMap(1);
        }
        else if(showResult == ShowResult.Skipped)
        {
            Debug.Log("Skipp");
        }
        else
        {
            Debug.Log("Filed");
        }
    }
}
