﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AITrack : MonoBehaviour
{
    [SerializeField]
    public Transform [] _allNp;

   
    public Transform NextPoint(int index)
    {
        if (index > _allNp.Length-1)
        {
            index = 0;
        }

        return _allNp[index];
    }
}
