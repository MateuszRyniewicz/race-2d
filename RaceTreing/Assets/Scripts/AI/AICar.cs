﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AICar : MonoBehaviour
{
    AITrack _aITrack;

    [SerializeField]
    int curretPoint;
    [SerializeField]
    Transform _toTargetPoint;


    [SerializeField]
    public float _speedAICar = 4f;
    [SerializeField]
    public float _aICarRotate = 10f;
    [SerializeField]
    GameObject _np;

    int curretLab = 0;

    void Start()
    {
        _aITrack = GameObject.FindObjectOfType<AITrack>();

        curretPoint = 0;
        _toTargetPoint = _aITrack._allNp[curretPoint];
        PointNewRandomizedNavPoint();
    }

    
    void Update()
    {
        Vector3 toTarget = _toTargetPoint.position - transform.position;
        float angle = Vector3.Angle(transform.up, toTarget);
        Vector3 cross = Vector3.Cross(transform.up, toTarget);
        if (cross.z < 0)
        {
            angle = -angle;
        }
        transform.Translate(0, _speedAICar*Time.deltaTime, 0);
        transform.Rotate(0, 0, angle*_aICarRotate*Time.deltaTime);

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform == _toTargetPoint)
        {
            curretPoint++;

            if (curretPoint > _aITrack._allNp.Length-1)
            {
                curretLab++;
            }
            int checkPointCount = (curretLab * _aITrack._allNp.Length) + curretPoint;
            System.Tuple<int, string> tempPlayer = LabSystem.Instance.players.Find(n => n.Item2 == gameObject.name);
            int indexOfTuple = LabSystem.Instance.players.FindIndex(x => x == tempPlayer);
            tempPlayer = new System.Tuple<int, string>(checkPointCount, gameObject.name);
            LabSystem.Instance.players.RemoveAt(indexOfTuple);
            LabSystem.Instance.players.Insert(indexOfTuple, tempPlayer);
            LabSystem.Instance.SortPlayer();
            curretPoint = (int)Mathf.Repeat(curretPoint, _aITrack._allNp.Length);
            _toTargetPoint = _aITrack.NextPoint(curretPoint);
            PointNewRandomizedNavPoint();
            Destroy(collision.gameObject, 20f);
        }
    }

    private void PointNewRandomizedNavPoint()
    {
        Vector2 NewPos = (Vector2)_toTargetPoint.position + Random.insideUnitCircle;
        GameObject _newNP = Instantiate(_np, NewPos, Quaternion.identity);
        _toTargetPoint = _newNP.transform;
    }
}
