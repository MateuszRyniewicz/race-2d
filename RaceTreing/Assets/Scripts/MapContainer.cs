﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapContainer : MonoBehaviour
{
    [SerializeField]
    public Transform [] _startPoints;

    [SerializeField]
    public CheckPoint[] _allPoints;
}
